using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemCollector : MonoBehaviour
{
    public int kiwis = 0;
    public int key = 0;
    [SerializeField] private Text kiwiText;
    [SerializeField] private Text keyText;
    [SerializeField] private AudioSource collectItemSound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Kiwi"))
        {
            collectItemSound.Play();
            Destroy(collision.gameObject);
            kiwis = kiwis + 10;
            kiwiText.text = "Kiwis: " + kiwis + "/80";
        }

        if (collision.gameObject.CompareTag("Key"))
        {
            collectItemSound.Play();
            Destroy(collision.gameObject);
            key = key + 1;
            keyText.text = "Collected keys: " + key + "/3";
        }
    }
}
