using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Finish : MonoBehaviour
{
    public ItemCollector collector;
    private bool isCompleted = false;
    private AudioSource finishingSound;
    // Start is called before the first frame update
    void Start()
    {    
        finishingSound = GetComponent<AudioSource>();
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Avatar" && !isCompleted && collector.key >= 3 && collector.kiwis >= 80)
        {
            Debug.Log(collector.key);
            finishingSound.Play();
            isCompleted = true;
            Invoke("FinishLvl",2f);
        }
    }

    private void FinishLvl()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
