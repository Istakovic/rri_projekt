using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    public GameObject loadingscreen;
    private Slider slider;
    public Text progressText;
    private float targetPosition = 0;
    public float Fillspeed = 0.5f;

    private void Awake()
    {
        slider = gameObject.GetComponent<Slider>();
    }
    public void LoadLevel(int sceneIndex)
    {
       StartCoroutine(LoadAsynchronusly(sceneIndex));
    }
    IEnumerator LoadAsynchronusly(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        loadingscreen.SetActive(true);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress/.9f);
            slider.value = progress;
            progressText.text = progress * 100f + "%";
            Debug.Log(progress);
            yield return null;
        }
    }
    private void Start()
    {
        IncrementProgress(0.75f); 
    }
    private void Update()
    {
        if(slider.value < targetPosition)
        {
            slider.value += Fillspeed * Time.deltaTime;
        }
    }

    public void IncrementProgress(float newProgress)
    {
        targetPosition= slider.value += newProgress;
    }
}
