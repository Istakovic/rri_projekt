using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AvatarLife : MonoBehaviour
{
    public int level = 1;
    public int health = 100;
    private Rigidbody2D rigidbody;
    private Animator animator;
    public Image[] lives;
    public int livesRemaining;


    [SerializeField] private AudioSource deathAudio;

    private void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Trap"))
        {
            LoseLife();
            transform.position = new Vector3(transform.position.x - Random.Range(1, 1), Random.Range(5, 1), 0);
        }
    }


    private void Die()
    {
        deathAudio.Play(); 
        rigidbody.bodyType = RigidbodyType2D.Static;
        animator.SetTrigger("death");
    }

    private void levelReset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoseLife()
    {
        deathAudio.Play();
        livesRemaining--;
        lives[livesRemaining].enabled = false;

        if (livesRemaining == 0)
        {
            Die();           
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Die();
        }
    }
}
