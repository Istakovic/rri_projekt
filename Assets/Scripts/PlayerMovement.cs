using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Animator animator;
    private new Rigidbody2D rigidbody;
    private SpriteRenderer sprite;
    private float directionX = 0f;
    private BoxCollider2D collider;

    private enum MovementState { idle,running,jumping,falling}

    [SerializeField] private AudioSource jumpAudio;
    [SerializeField] private float movementSpeed = 7f;
    [SerializeField] private float jump = 14f;
    [SerializeField] private LayerMask groundToJump;

    // Start is called before the first frame update
    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        collider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    private void Update()
    {
        directionX = Input.GetAxis("Horizontal");
        rigidbody.velocity = new Vector2(directionX * movementSpeed, rigidbody.velocity.y);

        if (Input.GetButtonDown("Jump") && isGrounded())
        {
            jumpAudio.Play();
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, jump);
        }
        RefreshAnimations();
    }

    private void RefreshAnimations()
    {
        MovementState state;
        if (directionX > 0f)
        {
            state = MovementState.running;
            sprite.flipX = false;
        }
        else if (directionX < 0f)
        {
            state = MovementState.running;
            sprite.flipX = true;
        }
        else
        {
            state = MovementState.idle;
        }

        if(rigidbody.velocity.y > .1f)
        {
            state = MovementState.jumping;
        }
        else if(rigidbody.velocity.y < -.1f)
        {
            state = MovementState.falling;
        }
        animator.SetInteger("state", (int)state);
    }

    private bool isGrounded()
    {
        return Physics2D.BoxCast(collider.bounds.center, collider.bounds.size, 0f, Vector2.down, .1f, groundToJump);
    }
}
