using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform avatar;
    
    private void Update()
    {
       transform.position = new Vector3(avatar.position.x, avatar.position.y, transform.position.z); 
    }
}
